<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminManageUsers extends Controller
{
    public function index()
    {
        $admin = User::latest()->paginate(5);
        return view("adminpanel", compact('admin'));
    }


    public function show($id)
    {
        $user = User::find($id);
        return view('show-user', compact('user'));
    }

    public function edit($id)
    {
        $admin = User::find($id);
        return view('edit-user', compact('admin'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => "required",
            "email" => "required|email|unique:users,id,".$id,
            "role" => "required",     ]);
        $admin = User::find($id);
        $admin-> update([
            "name" => $request -> name,
            "email" => $request -> email,
            "role" => $request -> role,
        ]);
        if(!is_null($admin))
            return back()->with("success", "Success! User updated");
        else
            return back()->with("failed", "Alert! User not updated");
    }

    public function destroy(User $admin)
    {
        $admin = $admin->delete();
        if(!is_null($admin))
            return back()->with("success", "Success! User deleted");
        else
            return back()->with("failed", "Alert! User not deleted");
    }
}
