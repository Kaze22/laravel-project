<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Students;

class StudentController extends Controller
{
    function index(){
        return view('addtostudents');
    }
    function addtostudents(Request $req)
    {
        $student = new Students;
        $student->sname=$req->input('sname');
        $student->class=$req->input('class');
        $student->boy=$req->input('boy');
        $validateData= $req->validate([
            'sname' => 'required|min:4|max:20',
            'class' => 'required'
        ]);
        $student->save();
        $req->session()->flash('status','Pizza data stored succesfully');
        return redirect('/');
    }
    function getstudents(){
        $data= Students::all();
        return view('getstudents',["data"=>$data]);
    }
}
