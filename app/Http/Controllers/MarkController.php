<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mark;

class MarkController extends Controller
{
    function index(){
        return view('addtomarks');
    }
    function addtomarks(Request $req)
    {
        $mark = new Mark;
        $mark->studentid=$req->input('studentid');
        $mark->mdate=$req->input('mdate');
        $mark->mark=$req->input('mark');
        $mark->type=$req->input('type');
        $mark->subjectid=$req->input('subjectid');
        $validateData= $req->validate([
            'studentid' => 'required|integer',
            'mdate' => 'required|date',
            'mark' => 'required|integer',
            'type' => 'required',
            'subjectid' => 'required|integer'
        ]);
        $mark->save();
        $req->session()->flash('status','Pizza data stored succesfully');
        return redirect('/');
    }
    function getmarks(){
        $data= Mark::all();
        return view('getmarks',["data"=>$data]);
    }
}
