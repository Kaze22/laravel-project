<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectController extends Controller
{
    function index(){
        return view('addtosubjects');
    }
    function addtosubjects(Request $req)
    {
        $subject = new Subject;
        $subject->sname=$req->input('sname');
        $subject->category=$req->input('category');
        $validateData= $req->validate([
            'sname' => 'required|min:4|max:20',
            'category' => 'required|min:4|max:20'
        ]);
        $subject->save();
        $req->session()->flash('status','Subject data stored succesfully');
        return redirect('/');
    }
    function getsubjects(){
        $data= Subject::all();
        return view('getsubjects',["data"=>$data]);
    }
}
