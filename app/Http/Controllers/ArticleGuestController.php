<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleGuestController extends Controller
{
    public function index()
    {
        $articles = Article::latest()->paginate(5);
        return view("articlesguest", compact('articles'));
    }
}
