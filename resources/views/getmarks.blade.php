@extends('layout')

@section('content')
<style>
  table{
    color:white;
  }
  th{
    color:black;
  }
  h1{
    text-align:center;
    padding-bottom:20px;
    padding-top:20px;
    color:black;
  }
</style>



<h1>Mark</h1>
<div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Id of the student</th>
      <th scope="col">Date</th>
      <th scope="col">Mark</th>
      <th scope="col">Type</th>
      <th scope="col">Subject id</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $item)
    <tr>
      <td>{{$item->studentid}}</th>
      <td>{{$item->mdate}}</td>
      <td>{{$item->mark}}</td>
      <td>{{$item->type}}</td>
      <td>{{$item->subjectid}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@stop