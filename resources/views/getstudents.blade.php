@extends('layout')

@section('content')
<style>
  table{
    color:black;
  }
  th{
    color:black;
  }
  h1{
    text-align:center;
    padding-bottom:20px;
    padding-top:20px;
    color:black;
  }
}
</style>



<h1>Students</h1>
<div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Name of the student</th>
      <th scope="col">Class</th>
      <th scope="col">boy/girl</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $item)
    <tr>
      <td>{{$item->sname}}</th>
      <td>{{$item->class}}</td>
      <td>{{$item->boy}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@stop