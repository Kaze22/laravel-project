@extends('layout')

@section('content')
<style>
  table{
    color:white;
  }
  th{
    color:black;
  }
  h1{
    text-align:center;
    padding-bottom:20px;
    padding-top:20px;
    color:black;
  }
}
</style>



<h1>Subjects</h1>
<div>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Category</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $item)
    <tr>
      <td>{{$item->sname}}</th>
      <td>{{$item->category}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@stop