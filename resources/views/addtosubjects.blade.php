@extends('layout')

@section('content')
@if($errors->any())
<style>
  .danger{
    color: red;
    font-size:24px;
  }
</style>
<ul>
  @foreach($errors->all() as $error)
  <li class="danger">{{$error}}</li>
  @endforeach
</ul>
@endif
<style>
  h1,label{
    color:black;
  }
</style>
<h1>Add to users table</h1>
<div class="col-sm-6">
<form action="" method="post">
    @csrf
    {{ method_field('PUT')}}
  <div class="form-group">
    <label>Subject name:</label>
    <input type="" class="form-control" id="sname" name="sname" placeholder="Enter your subject's name" required>
  </div>
  <div class="form-group">
    <label>Subject category</label>
    <input type="" class="form-control" id="caregory" name="category" placeholder="Enter your subject's category" required>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop