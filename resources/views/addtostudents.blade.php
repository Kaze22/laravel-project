@extends('layout')

@section('content')
@if($errors->any())
<style>
  .danger{
    color: red;
    font-size:24px;
  }
</style>
<ul>
  @foreach($errors->all() as $error)
  <li class="danger">{{$error}}</li>
  @endforeach
</ul>
@endif
<style>
  h1,label{
    color:black;
  }
</style>
<h1>Add to users table</h1>
<div class="col-sm-6">
<form action="" method="post">
    @csrf
    {{ method_field('PUT')}}
  <div class="form-group">
    <label>Student name:</label>
    <input type="" class="form-control" id="sname" name="sname" placeholder="Enter your students's name" required>
  </div>
  <div class="form-group">
    <label>Your student's class</label>
    <input type="" class="form-control" id="class" name="class" placeholder="Enter your students's class" required>
  </div>
  <div class="form-group">
    <label for="form-control">boy/girl(0 boy, -1 girl)</label>
    <select class="form-control" id="boy" name="boy">
        <option value="0">0</option>
        <option value="-1">-1</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop