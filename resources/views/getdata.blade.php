@extends('layout')

@section('content')
<style>
  h1,label{
    color:black;
  }
</style>
<h1>What table do you want to see?</h1>
<div class="col-sm-6">
<form>
    <div class="form-group">
    <label>Database you want to see</label>
    <input type="text" class="form-control" id="class" name="class" required>
  </div>
  <button id="submit" onClick="myOnclickFn()" type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
<script>
    function myOnClickFn(){
        document.location.href="";
    }

</script>
@stop