@extends('layout')

@section('content')
<style>
    h1{
        color:black;
        text-align:center;
    }
    #padding{
        padding-bottom:150px;
        
    }
    #infos{
        text-align:center;
    }
    #a{
        color:black;
        font-size:30px;
    }
</style>
<h1>Which table do you want to add data to?</h1>
<div>
    <ul id="padding"></ul>
<div id="infos">
<a id="a" href="/addtomarks"> Add to marks </a>
</div>
<ul id="padding"></ul>
<div id="infos">
<a id="a" href="/addtosubjects"> Add to subjects </a>
</div>
<ul id="padding"></ul>
<div id="infos">
<a id="a" href="/addtostudents"> Add to students </a>
</div>
</div>
@stop