@extends('layout')

@section('content')
@if($errors->any())
<ul>
  @foreach($errors->all() as $error)
  <li class="danger">{{$error}}</li>
  @endforeach
</ul>
@endif
<style>
  h1,label{
    color:black;
  }
</style>
<h1>Add to marks table</h1>
<div class="col-sm-6">
<form action="" method="post">
    @csrf
    {{ method_field('PUT')}}
  <div class="form-group">
    <label>Student id</label>
    <input type="" class="form-control" id="studentid" name="studentid" placeholder="Enter your students's id" required pattern = "[0-999]">
  </div>
  <div class="form-group">
    <label>Date</label>
    <input type="" class="form-control" id="mdate" name="mdate" placeholder="Enter date" required pattern = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]">
  </div>
  <div class="form-group">
    <label>Mark</label>
    <input type="" class="form-control" id="mark" name="mark" placeholder="Enter mark" required pattern = "[1-5]">
  </div>
  <div class="form-group">
    <label>Type</label>
    <input type="" class="form-control" id="type" name="type" placeholder="Enter type" required>
  </div>
  <div class="form-group">
    <label>Subject id</label>
    <input type="" class="form-control" id="subjectid" name="subjectid" placeholder="Enter subject id" required pattern = "[0-21]">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop