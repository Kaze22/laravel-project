@extends("layout")
@section("title") Show User @endsection
@section("content")
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 text-right">
            <a href="{{route('adminpanel.index')}}" class="btn btn-danger"> Back to AdminPanel </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
            <div class="card shadow">
                <div class="card-header">
                    <h4 class="card-title"> Show User </h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title"> Name </label>
                        <input type="text" readonly name="Name" class="form-control" id="name" value="@if(!empty($user)) {{$user->name}} @endif">
                    </div>
                    <div class="form-group">
                        <label for="description"> Email </label>
                        <textarea class="form-control" readonly name="email" id="email">@if(!empty($user)) {{$user->email}} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label for="description"> Role </label>
                        <textarea class="form-control" readonly name="role" id="role">@if(!empty($user)) {{$user->role}} @endif</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection