@extends('layout')
@extends('layouts.app2')

@section('content')
<?php if (Auth::user()->role==1){?>
    <p><strong>The admin panel is ready!</strong></p>
<h2>Bootstrap table</h2>
<div class="row mb-4">
    <div class="col-xl-6">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('failed'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('failed')}}
            </div>
        @endif 
    </div>
</div>
<table class="table table-striped">
    <thead>
        <th> Id </th>
        <th> Name </th>
        <th> Email </th>
        <th> Role </th>
    </thead>
    <tbody>
            @foreach($admin as $adm)
                <tr>
                    <td> {{$adm->id}} </td>
                    <td> {{$adm->name}} </td>
                    <td> {{$adm->email}} </td>
                    <td> {{$adm->role}} </td>
                    <td>
                        <form action="{{route('adminpanel.destroy', $adm->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('adminpanel.show', $adm->id)}}" class="btn btn-sm btn-info"> View </a>
                            <a href="{{route('adminpanel.edit', $adm->id)}}" class="btn btn-sm btn-success"> Edit </a>
                            <button type="submit" class="btn btn-sm btn-danger"> Delete </button>
                        </form>
                    </td>
                </tr>
            @endforeach
    </tbody>
</table>

                        <?php } ?> 

@endsection