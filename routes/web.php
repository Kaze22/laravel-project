<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\MarkController;
use App\Http\Controllers\ArticleGuestController;
use App\Http\Controllers\AdminManageUsers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','App\Http\Controllers\HomeController@index');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('introduction', function () { return view('introduction'); })->name("introduction");
Route::group(['middleware' => 'auth2'], function () {     
    Route::get('user', function () { return view('user'); })->name("user");
    Route::get('admin', function () { return view('admin'); })->name("admin");
    //Route::get('/test1', function () { return view('test1'); })->name('test1');
    Route::resource('adminpanel', AdminManageUsers::class); 
    //Route::get('adminpanel', [AdminManageUsers::class, 'adminpanel'])->name('adminpanel');
    //Route::get('adminpanel/{admin}',[AdminManageUser::class,'show'])->name('adminpanel.show');
    Route::get('/test2', function () { return view('test2'); })->name('test2'); 
    Route::resource('articles', ArticleController::class);
    Route::view('/addtostudents','addtostudents');
    Route::put('/addtostudents','App\Http\Controllers\Studentcontroller@addtostudents');
    Route::view('/addtosubjects','addtosubjects');
    Route::put('/addtosubjects','App\Http\Controllers\Subjectcontroller@addtosubjects');
    Route::view('/addtomarks','addtomarks');
    Route::put('/addtomarks','App\Http\Controllers\Markcontroller@addtomarks');
    Route::view('/addto','addto');
    Route::view('/getdata','getdata');
    Route::get('/getstudents','App\Http\Controllers\Studentcontroller@getstudents');
    Route::get('/getmarks','App\Http\Controllers\Markcontroller@getmarks');
    Route::get('/getsubjects','App\Http\Controllers\Subjectcontroller@getsubjects');
    
});
//Route::resource('adminpanel', AdminManageUsers::class); 
Route::resource('articlesguest', ArticleGuestController::class);

